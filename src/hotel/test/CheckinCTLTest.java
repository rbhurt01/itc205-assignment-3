package hotel.test;

import hotel.HotelHelper;
import hotel.booking.BookingUI;
import hotel.checkin.CheckinCTL;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.lang.reflect.Executable;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CheckinCTLTest {
    CheckinCTL control;
    Hotel hotel;

    @BeforeEach
    final void setUp() throws Exception{
        hotel = HotelHelper.loadHotel();
        control = new CheckinCTL(hotel);
        Room room= new Room(201,RoomType.DOUBLE);
        Guest guest= new Guest("Fred", "Nurke", 2);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date;
        date= format.parse("11-12-2001");
        CreditCard creditCard= new CreditCard(CreditCardType.VISA, 2, 2);
        //Booking booking = new Booking(guest,room,date,2,2,creditCard);
        room.book(guest,date,2,2,creditCard);
        room.checkin();
//        Executable e = () -> booking.checkin();
//        Throwable t = assertThrows(RuntimeException.class,e);
//        assertEquals("Cannot checkIn, Room is not pending",t.getMessage());


    }

    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    final void testCheckInDetailsIsValid(){
        control.confirmationNumberEntered(123435);
        control.checkInConfirmed(true);
        control.completed();
        control.cancel();
        control.run();
        assertEquals(1,hotel.bookingsByConfirmationNumber.size());
    }


}