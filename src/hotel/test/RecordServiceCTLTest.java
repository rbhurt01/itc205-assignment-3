package hotel.test;

import hotel.HotelHelper;
import hotel.booking.BookingCTL;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
import hotel.service.RecordServiceCTL;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RecordServiceCTLTest {
    RecordServiceCTL control;
    Hotel hotel;
    BookingCTL bookingCTL;
    Booking booking;

    @BeforeEach
    void setUp() throws Exception {
        hotel = HotelHelper.loadHotel();
        //Room room= new Room(201,RoomType.DOUBLE);
//        Guest guest= new Guest("Fred", "Nurke", 2);
//        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//        Date date;
//        date= format.parse("11-12-2001");
//        CreditCard creditCard= new CreditCard(CreditCardType.VISA, 2, 2);
//        room.book(guest, date, 4, 1,  creditCard);
       control = new RecordServiceCTL(hotel);
    }

    @AfterEach
    void tearDown() throws Exception {
    }


    @Test
    final void testRecordServiceDetailsIsValid(){

        control.roomNumberEntered(301);
        control.serviceDetailsEntered(ServiceType.RESTAURANT,600.0);
        control.completed();
        control.cancel();
        control.run();


        assertEquals(1,hotel.bookingsByConfirmationNumber.size());
    }
}