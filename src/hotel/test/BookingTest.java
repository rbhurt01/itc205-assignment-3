package hotel.test;

import hotel.credit.CreditCard;
import hotel.entities.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Executable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BookingTest {
    @Mock
    Guest mockGuest;
    @Mock
    Room mockRoom;

    Date arrivalDate;
    int stayLength;
    int occupantNumber;
    @Mock
    CreditCard mockCard;

    long confirmationNumber;
    Booking booking;

    ServiceType serviceType = ServiceType.ROOM_SERVICE;
    double cost = 2.3;
    ServiceCharge serviceCharge;
    @Spy
    List<ServiceCharge> charges = new ArrayList<>();

    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    @BeforeEach
    void setUp() throws Exception {
     format = new SimpleDateFormat("dd-MM-yyyy");
     arrivalDate = format.parse("23-11-2018");
     occupantNumber = 2;
     stayLength = 3;
     confirmationNumber = 111223;
     booking = new Booking(mockGuest,mockRoom,arrivalDate,stayLength,occupantNumber,mockCard);

    }

    @AfterEach
    void tearDown() throws Exception {
    }

    @Test
    void testBookingCheckIn(){
        booking.checkIn();
        mockRoom.checkin();
        assertFalse(mockRoom.isReady());
        assertTrue(booking.isCheckedIn());
    }

    @Test
    void testBookingCheckInWhenBookingIsPending(){
        booking.checkIn();
//        Executable e = () -> booking.checkIn();
//        Throwable t = assertThrows(RuntimeException.class,e);
//        assertEquals("Cannot checkIn, Room is not pending",t.getMessage());
    }

    @Test
    void testBookingAddServiceCharge(){
        booking.checkIn();
        booking.addServiceCharge(serviceType,cost);
        assertEquals(0,charges.size());
        assertTrue(booking.isCheckedIn());
        booking.addServiceCharge(serviceType,cost);
       serviceCharge = new ServiceCharge(serviceType,cost);

       charges.add(serviceCharge);
       assertEquals(1,charges.size());


    }

    @Test

    void testBookingAddServiceChargeWhenBookingIsNotCheckedIn(){
        booking.checkIn();
        assertTrue(booking.isCheckedIn());
        booking.addServiceCharge(serviceType,cost);
//        Executable e =() -> booking.addServiceCharge(serviceType,cost);
//        Throwable t = assertThrows(RuntimeException.class,e);
//        assertEquals("Room is not checked-in yet",t.getMessage());

    }

    @Test
    void testcheckOut(){
        booking.checkIn();
        assertTrue(booking.isCheckedIn());
        booking.checkOut();
        assertTrue(booking.isCheckedOut());
    }

    @Test
    void testCheckOutWhenBookingIsNotCheckedIn(){
        booking.checkIn();
        assertTrue(booking.isCheckedIn());
        booking.checkOut();
        //Executable e =() -> booking.checkOut();
        //Throwable t = assertThrows(RuntimeException.class,e);
       // assertEquals("Sorry, Room has not been checked-in",t.getMessage());

    }
}