package hotel.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hotel.credit.CreditCard;
import hotel.utils.IOUtils;

public class Hotel {
	
	private Map<Integer, Guest> guests;
	public Map<RoomType, Map<Integer,Room>> roomsByType;
	public Map<Long, Booking> bookingsByConfirmationNumber;
	public Map<Integer, Booking> activeBookingsByRoomId;
	
	
	public Hotel() {
		guests = new HashMap<>();
		roomsByType = new HashMap<>();
		for (RoomType rt : RoomType.values()) {
			Map<Integer, Room> rooms = new HashMap<>();
			roomsByType.put(rt, rooms);
		}
		bookingsByConfirmationNumber = new HashMap<>();
		activeBookingsByRoomId = new HashMap<>();
	}

	
	public void addRoom(RoomType roomType, int id) {
		IOUtils.trace("Hotel: addRoom");
		for (Map<Integer, Room> rooms : roomsByType.values()) {
			if (rooms.containsKey(id)) {
				throw new RuntimeException("Hotel: addRoom : room number already exists");
			}
		}
		Map<Integer, Room> rooms = roomsByType.get(roomType);
		Room room = new Room(id, roomType);
		rooms.put(id, room);
	}

	
	public boolean isRegistered(int phoneNumber) {
		return guests.containsKey(phoneNumber);
	}

	
	public Guest registerGuest(String name, String address, int phoneNumber) {
		if (guests.containsKey(phoneNumber)) {
			throw new RuntimeException("Phone number already registered");
		}
		Guest guest = new Guest(name, address, phoneNumber);
		guests.put(phoneNumber, guest);		
		return guest;
	}

	
	public Guest findGuestByPhoneNumber(int phoneNumber) {
		Guest guest = guests.get(phoneNumber);
		return guest;
	}

	
	public Booking findActiveBookingByRoomId(int roomId) {
		Booking booking = activeBookingsByRoomId.get(roomId);
		return booking;
	}


	public Room findAvailableRoom(RoomType selectedRoomType, Date arrivalDate, int stayLength) {
		IOUtils.trace("Hotel: checkRoomAvailability");
		Map<Integer, Room> rooms = roomsByType.get(selectedRoomType);
		for (Room room : rooms.values()) {
			IOUtils.trace(String.format("Hotel: checking room: %d",room.getId()));
			if (room.isAvailable(arrivalDate, stayLength)) {
				return room;
			}			
		}
		return null;
	}

	
	public Booking findBookingByConfirmationNumber(long confirmationNumber) {
		return bookingsByConfirmationNumber.get(confirmationNumber);
	}

	
	public long book(Room room, Guest guest, 
			Date arrivalDate, int stayLength, int occupantNumber,
			CreditCard creditCard) {
		// completed
		Date currentDate = new Date();
		Date checkoutDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(checkoutDate);
		c.add(Calendar.DATE, 5);
		checkoutDate = c.getTime();
		System.out.println(checkoutDate.getDate());

		room.book(guest, arrivalDate, stayLength, occupantNumber, creditCard);
		//if(currentDate == arrivalDate || currentDate == arrivalDate.after(stayLength))
		Booking booking = new Booking(guest, room, arrivalDate,stayLength, occupantNumber, creditCard);
		bookingsByConfirmationNumber.put(booking.getConfirmationNumber(), booking);
		return booking.getConfirmationNumber();
	}

	
	public void checkin(long confirmationNumber) {
		// TODO Auto-generated method stub
<<<<<<< HEAD
//		if (bookingsByConfirmationNumber.get(confirmationNumber) != null) {
//
//		} else {
//			// No such key
=======
//		Booking booking= bookingsByConfirmationNumber.get(confirmationNumber);
//		if (booking != null) {
//			booking.getRoomId();
//			booking.checkIn();
//		} else {
//			throw new RuntimeException("Booking doesn't exists");
>>>>>>> rohit
//		}
	}


	public void addServiceCharge(int roomId, ServiceType serviceType, double cost) {
		// TODO Auto-generated method stub
//		Booking booking= activeBookingsByRoomId.get(roomId);
//		if (booking != null){
//			booking.addServiceCharge(serviceType, cost);
//		}
//		else{
//			throw new RuntimeException("Booking does not exist!");
//		}


	}

	
	public void checkout(int roomId) {
		// TODO Auto-generated method stub
	}


}
